server {

    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.%DOMAIN%;

    ssl_certificate /var/www/%DOMAIN%/_/fullchain.pem;
    ssl_certificate_key /var/www/%DOMAIN%/_/privkey.pem;

    return 301 $scheme://%DOMAIN%$request_uri;

}

server {

    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name %DOMAIN%;

    root /var/www/%DOMAIN%/public;
    index index.php;

    location ~ ^/(index|matomo|piwik|js/index|plugins/HeatmapSessionRecording/configs)\.php$ {

        fastcgi_split_path_info ^(.+?\.php)(/.*)$;

        try_files $fastcgi_script_name =404;

        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $request_filename;
        fastcgi_param HTTP_PROXY ""; # https://httpoxy.org/

        fastcgi_index index.php;

        fastcgi_keep_conn on;
        fastcgi_read_timeout 30s;
        fastcgi_send_timeout 30s;

        fastcgi_pass unix:/run/php/php7.4-fpm.%DOMAIN%.sock;

    }

    # Custom error page
    # error_page 404 /404.html;

    ssl_certificate /var/www/%DOMAIN%/_/fullchain.pem;
    ssl_certificate_key /var/www/%DOMAIN%/_/privkey.pem;

    # Logging
    access_log /var/www/%DOMAIN%/_/logs/access.log json_log_format buffer=16k flush=1m;
    error_log /var/www/%DOMAIN%/_/logs/error.log;

    # Common configuration
    include /etc/nginx/snippets/serverkit-gzip-compression.conf;
    include /etc/nginx/snippets/serverkit-disable-logging-common-files.conf;

    # Headers

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy
    add_header Referrer-Policy origin always;

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
    add_header X-Content-Type-Options nosniff always;

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
    add_header X-Frame-Options SAMEORIGIN always;

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Timing-Allow-Origin
    add_header Timing-Allow-Origin "*" always;

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
    add_header Access-Control-Allow-Origin "*";

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cross-Origin-Resource-Policy
    add_header Cross-Origin-Resource-Policy "cross-origin" always;

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cross-Origin-Embedder-Policy
    add_header Cross-Origin-Embedder-Policy "require-corp" always;

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cross-Origin-Opener-Policy
    add_header Cross-Origin-Opener-Policy "same-origin" always;

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
    add_header Content-Security-Policy "default-src https: 'unsafe-eval' 'unsafe-inline'" always;

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Feature-Policy
    # Features: https://github.com/w3c/webappsec-permissions-policy/blob/main/features.md
    add_header Permissions-Policy "accelerometer=(self), ambient-light-sensor=(self), autoplay=(self), battery=(self), camera=(self), cross-origin-isolated=(self), display-capture=(self), document-domain=(self), encrypted-media=(self), execution-while-not-rendered=(self), execution-while-out-of-viewport=(self), fullscreen=(self), geolocation=(self), gyroscope=(self), magnetometer=(self), microphone=(self), midi=(self), navigation-override=(self), payment=(self), picture-in-picture=(self), publickey-credentials-get=(self), screen-wake-lock=(self), sync-xhr=(self), usb=(self), web-share=(self), xr-spatial-tracking=(self)" always;

    #########################
    # Custom Matomo entries #
    #########################

    location ~* ^.+\.php$ {
        deny all;
        return 403;
    }

    location ~ ^/(config|tmp|core|lang) {
        deny all;
        return 403;
    }

    location ~ /\.ht {
        deny  all;
        return 403;
    }

    # The next sections duplicate the headers described above because even if in Nginx add_header statements are
    # inherited by default, they are not if the current configuration level (the location blocks below) contain a
    # call to add_header. The headers in the original Matomo template and the duplicated headers are separated by an
    # empty line.
    # http://nginx.org/en/docs/http/ngx_http_headers_module.html#add_header

    location ~ js/container_.*_preview\.js$ {
        expires off;
        add_header Cache-Control 'private, no-cache, no-store';

        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
        add_header Referrer-Policy origin always;
        add_header X-Content-Type-Options nosniff always;
        add_header X-Frame-Options SAMEORIGIN always;
        add_header Timing-Allow-Origin "*" always;
        add_header Access-Control-Allow-Origin "*";
        add_header Cross-Origin-Resource-Policy "cross-origin" always;
        add_header Cross-Origin-Embedder-Policy "require-corp" always;
        add_header Cross-Origin-Opener-Policy "same-origin" always;
    }

    location ~ \.(gif|ico|jpg|png|svg|js|css|htm|html|mp3|mp4|wav|ogg|avi|ttf|eot|woff|woff2|json)$ {
        allow all;
        expires 1h;
        add_header Pragma public;
        add_header Cache-Control "public";

        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
        add_header Referrer-Policy origin always;
        add_header X-Content-Type-Options nosniff always;
        add_header X-Frame-Options SAMEORIGIN always;
        add_header Timing-Allow-Origin "*" always;
        add_header Access-Control-Allow-Origin "*";
        add_header Cross-Origin-Resource-Policy "cross-origin" always;
        add_header Cross-Origin-Embedder-Policy "require-corp" always;
        add_header Cross-Origin-Opener-Policy "same-origin" always;
    }

    location ~ ^/(libs|vendor|plugins|misc|node_modules) {
        deny all;
        return 403;
    }

    location ~/(.*\.md|LEGALNOTICE|LICENSE) {
        default_type text/plain;
    }

}
