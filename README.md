# ServerKit

A set of guides and shell scripts that automate the configuration and provisioning of Debian 11 servers. The philosophy here is to honor and use Debian conventions and defaults, and to keep our opinions to a minimum.

## Configuring SSH access

SSH configuration is done manually. Use `nano /etc/ssh/sshd_config` to modify the configuration file. When all the changes are done, restart the SSH service by using `systemctl restart sshd` and check that you can connect to the remote machine.

### Set up public/private key authentication

Most providers offer a way to set up key authentication while creating a VPS. We are going to make use of this feature. To create a new SSH key run this command on *your* machine:

```
ssh-keygen -t ed25519 -C "comment"
```

> ☑ SSH keys are typically issued for a single machine. Machine name can be stated in the comment. Alternatively, an email address can also be used as the comment, if you prefer to treat SSH keys as personal.

> ⚠ Remember to add a passphrase to the key. If copying and pasting the key into your machine, remember that the `.ssh` folder must have permissions at 700 and the key files themselves at 400.

If you want to instead use an existing SSH key, display it using this command, copy it and then paste it into the provider's configuration screen:

```
cat ~/.ssh/id_ed25519.pub
```

> ⚠ The key that must be copied into the server is the public key.

### Change default-port

This **will not** make the setup more secure, but it will help to stay away from automated login attempts in the default port, reducing both CPU usage by a little and noise in logs by a lot.

Add/uncomment `Port 22` and change to another port number.

> ☑ Find yourself a suitable port avoiding commonly used ports using the [port number list in Wikipedia](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers).

### Securing SSH access

Ideally you should disable root login and plain text passwords:

For this, you should have a unprivileged user that can use `sudo` to get privileges.

- Install sudo with `apt-get install sudo`.
- Create a new user with `adduser user`, where 'user' is the new username
- Add that user to the `sudo` group with `adduser user sudo`.

> ⚠ Do not forget to create/copy a SSH key for the new user too. You can do this with `ssh-copy-id user@remotemachine` (complete with your information) You may need to log in with the user's password before being able to add a key.

Check that you are able to connect to the remote machine using the new user and its key, that you can in fact launch commands with privileges using `sudo` and configure SSH:

```
PermitRootLogin no
PasswordAuthentication no
```

## Create a swap file

Assuming a swap file is needed for the server, you can create one following these steps. Elevate yourself to root to run these commands:

```shell
touch /swapfile

# Only root, please.
chmod 600 /swapfile

# count equals to the size of the file, in MiB (1024 will create a 1 GiB file)
dd if=/dev/zero of=/swapfile bs=1M count=1024

mkswap /swapfile
swapon /swapfile
```

## Installing & using ServerKit

```shell
cd /opt
git clone https://codeberg.org/Mootex/ServerKit.git
bash ServerKit/scripts/configure-misc.sh
```

> ⚠ Note that right now the kit assumes that it will be installed in `/opt/ServerKit`.

Now you can run the scripts located in the `scripts` folder.
