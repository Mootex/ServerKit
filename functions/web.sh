#!/usr/bin/env bash

web::create_web_dir() {
    # $1: Domain name

    mkdir -p "/var/www/$1"
    mkdir -p "/var/www/$1/public"
    mkdir -p "/var/www/$1/_"
    mkdir -p "/var/www/$1/_/logs"
    mkdir -p "/var/www/$1/_/backups"

    local html="/var/www/$1/public/index.html"

    if [ ! -f "$html" ]; then
        echo "$1 is working!" > "$html"
    fi

}

web::create_web_user() {
    # $1: Domain name
    # $2: user name
    # $3: uid (will also be used as gid)

    # Create a group for the user.
    # -g | The gid, guessed before.
    # -r | Create a system group.
    groupadd    \
        -g "$3" \
        -r      \
        "$2"

    # Create the web user.
    # -c | Full domain name used as the comment.
    # -b | Base directory for home.
    # -d | Full path of the home directory. This is NOT CREATED here.
    # -u | The uid, guessed before.
    # -g | The gid, same as uid, guessed before.
    # -s | Login shell of the new account
    # -l | Do not add the user to recent login db.
    # -r | Create a system user.
    useradd              \
        -c "$1"          \
        -b "/var/www"    \
        -d "/var/www/$1" \
        -u "$3"          \
     -N -g "$3"          \
        -s "/bin/bash"   \
        -lr              \
        "$2"

}

web::get_suitable_uid() {
    # Obtains a UID for a web user in the range 10000-19999.
    # $1: Path to the file with users in passwd format.

    local result=10000

    local list=$(cat "$1" | cut -d":" -f3 | sort -rn)

    for number in $list; do

        if [ $number -ge 20000 ]; then
            continue
        fi

        if [ $number -ge 10000 ]; then
            result=$(($number+1))
            break
        else
            break
        fi

    done

    echo $result
}

web::apply_www_permissions() {
    # $1: domain name
    # $2: user name

    chown -R $2:www-data "/var/www/$1"
    chmod -R o-rwx "/var/www/$1"
    chmod -R g-rwx "/var/www/$1/_"
}
