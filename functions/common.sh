#!/usr/bin/env bash

common_config_folder="/opt/ServerKit/configuration"
common_backup_folder="/opt/ServerKit.Backups"

common_template_folder="/opt/ServerKit/templates"

common_settings_username_length=24
common_settings_username_suffix_length=5

common::check_display_help() {
    # Displays the script help if the user is requesting it.
    # Assumes a function called script::help is defined.

    if [ "$1" == "help" ]; then
        script::help
    fi

}

common::require_root() {
    # Stops the execution if the script was launched without privileges.

    if [ $EUID -ne 0 ]; then
        echo "This script needs to be executed as root. Aborting."
        exit
    fi
}

common::require_argn() {
    # Stops the execution if the script is not given enough arguments.
    # Assumes a function called script::help is defined.

    if [ $# -ne 2 ]; then
        echo "ERROR: bad usage of common::require_argn()"
        exit 1
    fi

    if [ $1 -eq 0 ]; then
        script::help
        exit 1
    fi

}

common::make_file_backup() {

    if [ ! -d "$common_backup_folder" ]; then
        mkdir "$common_backup_folder"
    fi

    let n=1
    while [ -f "$common_backup_folder$1.$n.bak" ]; do
        let n++
    done

    local full_file_name
    full_file_name="$common_backup_folder$1.$n.bak"

    local full_file_path
    full_file_path=$(dirname $full_file_name)

    if [ ! -d "$full_file_path" ]; then
        mkdir -p "$full_file_path"
    fi

    cp "$1" "$full_file_name"

    echo "    Backup file '$full_file_name' was created."

}

common::install_config() {

    local template="$common_config_folder$1"

    if [ ! -f "$template" ]; then
        echo "    Can't find configuration template: '$template'"
        return
    fi

    common::make_file_backup $1

    if [ -f "$1" ]; then
        rm $1
    fi

    cp "$common_config_folder$1" "$1"

}

common::install_custom_config() {
    # Copies a configuration template and replaces variables with the given data.
    # Template must have variables wrapped with %, example: %VAR1%
    # $1: Source template file
    # $2: Destination for the file
    # $3: Variables, in format "VAR1=value|VAR2=value|VAR3=value"

    local template="$common_template_folder$1"

    if [ ! -f "$template" ]; then
        echo "    Can't find configuration template: '$template'"
        return
    fi

    cp "$template" "$2"

    readarray -d "|" -t variables <<< "$3"

    for (( i=0; i < ${#variables[*]}; i++)); do

        readarray -d "=" -t line <<< "${variables[i]}"

        key="${line[0]}"

        value="${line[1]}"
        value="${value//[$'\t\r\n']}"

        sed -i "s/%$key%/$value/g" "$2"

    done

}

common::filter_domain_name() {
    echo "$1" | sed "s/\./_/g"
}

common::find_user_for_domain_or_fail() {

    local found=$(grep "$1" /etc/passwd | cut -f1 -d":")

    if [ -z "$found" ]; then
        echo "Could not find username for domain '$1'."
        exit 1
    fi

    echo "$found"
}

common::domain_to_username() {

    local basename_length
    basename_length=$(($common_settings_username_length - $common_settings_username_suffix_length - 1))

    local new_suffix_length
    new_suffix_length=$common_settings_username_suffix_length

    local filtered_name=$(common::filter_domain_name $1)

    if [ ${#filtered_name} -gt $basename_length ]; then

        # Shorten username to fit max length
        filtered_name=${filtered_name:0:$basename_length}

        # Remove ending _ to avoid __ in resulting username
        if [ "${filtered_name: -1}" == "_" ]; then
            filtered_name=${filtered_name::-1}
            # Add one to suffix length to compensate for the removed _
            let new_suffix_length++
        fi

    else

        local missing
        missing=$(($basename_length - ${#filtered_name}))

        let new_suffix_length+=$missing

    fi

    local suffix="_$(head /dev/urandom | tr -dc a-z0-9 | head -c $new_suffix_length ; echo '')"

    echo "$filtered_name$suffix"

}

common::check_package_installed() {
    # Checks if a package is installed
    # $1: package name
    # return: "true" if the package is installed, "false" otherwise

    local output=$(dpkg -l $1 2>&1 | grep "no packages found")

    if [ ${#output} -eq 0 ]; then
        echo "true"
    else
        echo "false"
    fi
}
