#!/usr/bin/env bash
# Requires: /dev/urandom

random::string() {
    # Creates a random string of the given length.
    # arg 1: Lenght of the resulting string. Not intended for long (> 100) strings.
    echo $(head /dev/urandom | tr -dc a-z0-9 | head -c $1)
}

random::temp_file() {
    # Creates an empty file with a random name.
    # arg 1: Prefix that will be added to the file name.
    if [ -z "$1" ]; then
        echo "/tmp/$(random::string 16)"
    else
        echo "/tmp/$1$(random::string 16)"
    fi
}