#!/usr/bin/env bash
source /opt/ServerKit/functions/common.sh

common::require_root

echo ">>> Removing motd..."
echo -n "" > /etc/motd
