#!/usr/bin/env bash
source /opt/ServerKit/functions/common.sh

common::require_root

echo ">>> Installing Nginx"
apt -qq install -y nginx certbot

echo ">>> Stopping Nginx"
systemctl stop nginx

echo ">>> Removing default site in Nginx"
unlink /etc/nginx/sites-available/default
unlink /etc/nginx/sites-enabled/default
rm -fR /var/www/html

echo ">>> Installing configuration for Nginx..."

common::install_config /etc/nginx/nginx.conf
common::install_config /etc/nginx/fastcgi.conf
common::install_config /etc/nginx/fastcgi_params
common::install_config /etc/nginx/mime.types

common::install_config /etc/nginx/conf.d/serverkit-cache-file-descriptors.conf
common::install_config /etc/nginx/conf.d/serverkit-ssl.conf

common::install_config /etc/nginx/snippets/serverkit-disable-logging-common-files.conf
common::install_config /etc/nginx/snippets/serverkit-gzip-compression.conf
common::install_config /etc/nginx/snippets/serverkit-http-cache.conf
common::install_config /etc/nginx/snippets/serverkit-http-headers.conf

common::install_config /etc/logrotate.d/nginx

created_dhparams=0
if [ ! -f "/etc/ssl/dhparam.pem" ]; then
    echo ">>> Generating Diffie Hellman ephemeral parameters"
    openssl dhparam -out /etc/ssl/dhparam.pem 128
    created_dhparams=1
fi

created_cert=0
if [ ! -f "/etc/ssl/private/nginx.pem" -o ! -f "/etc/ssl/private/nginx.key" ]; then
    openssl req -new -x509 -days 365 -nodes -out /etc/ssl/private/nginx.pem -keyout /etc/ssl/private/nginx.key -subj '/CN=localhost'
    created_cert=1
fi

echo ">>> Starting Nginx"
systemctl start nginx

echo "[!] Please install a SSL certificate"

if [ $created_dhparams -eq 1 ]; then
    echo "    A self signed certificate has been created."
    echo "    Generate a valid, signed certificate and add it in /etc/nginx/nginx.conf"
fi

if [ $created_dhparams -eq 1 ]; then
    echo "[!] Simple Diffie Hellman ephemeral parameters have been created."
    echo "    Run 'openssl dhparam -out /etc/ssl/dhparam.pem 4096' to generate strong params."
    echo "    The command is going to take a LONG time."
fi
