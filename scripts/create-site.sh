#!/usr/bin/env bash
source /opt/ServerKit/functions/common.sh
source /opt/ServerKit/functions/web.sh

script::help() {
    echo "Creates the required configuration and filesystem structure to host a website."
    echo "    A new user for the website will be created too."
    echo
    echo "Usage: $0 domain"
    echo "    domain           The domain of the site to create"
    echo
    echo "Examples:"
    echo "    $0 mywebsite.com"
    echo "    $0 blog.mywebsite.com"
}

common::check_display_help $1
common::require_root
common::require_argn $# 1

username=$(common::domain_to_username $1)
uid=$(web::get_suitable_uid /etc/passwd)

web::create_web_dir $1
web::create_web_user $1 $username $uid

web::apply_www_permissions $1 $username

destination_config_file="/etc/nginx/sites-available/$1.conf"

common::install_custom_config                  \
    "/etc/nginx/sites-available/new-site.conf" \
    "$destination_config_file"                 \
    "DOMAIN=$1"

echo ">>> Site '$1' has been CREATED."
echo "    Directory:      /var/www/$1"
echo "    Nginx config:   $destination_config_file"
echo "    User:           $username"
echo "    Group:          $username"
