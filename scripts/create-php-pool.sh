#!/usr/bin/env bash
source /opt/ServerKit/functions/common.sh
source /opt/ServerKit/functions/web.sh

script::help() {
    echo "Creates a PHP FPM pool with the given name."
    echo
    echo "Usage: $0 domain"
    echo "    domain           The domain of the site to create"
    echo
    echo "Examples:"
    echo "    $0 mywebsite.com"
    echo "    $0 blog.mywebsite.com"
}

common::check_display_help $1
common::require_root
common::require_argn $# 1

username=$(common::find_user_for_domain_or_fail $1)

destination_config_file="/etc/php/7.4/fpm/pool.d/$1.conf"

common::install_custom_config                \
    "/etc/php/7.4/fpm/pool.d/new-pool.conf"  \
    "$destination_config_file"               \
    "DOMAIN=$1|USERNAME=$username"

systemctl restart php7.4-fpm

echo ">>> Pool has been CREATED."
echo "    Pool config:    $destination_config_file"
echo "    User:           $username"
