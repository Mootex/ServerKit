#!/usr/bin/env bash
source /opt/ServerKit/functions/common.sh

common::require_root

echo ">>> Installing PHP"
apt -qq install -y php7.4 php7.4-fpm php7.4-curl php7.4-gd php7.4-intl php7.4-mbstring php7.4-mysql php7.4-xml php7.4-zip

echo ">>> Installing configuration for PHP..."

common::install_config /etc/php/7.4/fpm/php.ini
common::install_config /etc/php/7.4/fpm/php-fpm.conf

unlink /etc/php/7.4/fpm/pool.d/www.conf
