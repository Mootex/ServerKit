#!/usr/bin/env bash
source /opt/ServerKit/functions/common.sh

common::require_root

echo ">>> Installing MariaDB"
apt -qq install -y mariadb-server

echo ">>> Stopping MariaDB"
systemctl stop mysql

echo ">>> Installing configuration for MariaDB..."

common::install_config /etc/mysql/mariadb.conf.d/50-server.cnf

echo ">>> Starting MariaDB"
systemctl start mysql
