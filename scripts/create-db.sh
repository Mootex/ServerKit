#!/usr/bin/env bash
source /opt/ServerKit/functions/common.sh

script::help() {
    echo "Creates a database in MariaDB and assigns username and password to it."
    echo
    echo "Usage: $0 database [user [password]]"
    echo "    database         Name of the database, usually the domain name"
    echo "    user             User name, will use domain name replacing . with _ if none specified"
    echo "    password         Your password for the database user, will generate a random one if none specified"
    echo
    echo "Examples:"
    echo "    $0 mysite.com"
    echo "    $0 mysite.com myuser"
    echo "    $0 mysite.com myuser p455w0rd"
}

common::check_display_help $1
common::require_root
common::require_argn $# 1

database=$(echo "$1" | sed -r 's/\./_/g')
db_user="$database"
db_pass=$(head -c 80 /dev/urandom | tr -dc 'a-zA-Z0-9!@#$%&*_-')

if [ ! -z $2 ]; then
    db_user="$2"
fi

if [ ! -z $3 ]; then
    db_pass="$3"
fi

mysql -e "CREATE DATABASE IF NOT EXISTS $database; GRANT ALL ON $database.* TO '$db_user'@'localhost' IDENTIFIED BY '$db_pass'; FLUSH PRIVILEGES;"

echo "Database '$database' has been CREATED.";
echo "    User:     $db_user"
echo "    Password: $db_pass"
