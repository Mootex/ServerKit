#!/usr/bin/env bash

set -e
set -u

source "/opt/ServerKit/tests/functions/test.sh"
source "/opt/ServerKit/functions/random.sh"
source "/opt/ServerKit/functions/web.sh"

test::assert_test_case "web::get_suitable_uid" "/opt/ServerKit/tests/cases/get_suitable_uid-1.txt"
test::assert_test_case "web::get_suitable_uid" "/opt/ServerKit/tests/cases/get_suitable_uid-2.txt"
test::assert_test_case "web::get_suitable_uid" "/opt/ServerKit/tests/cases/get_suitable_uid-3.txt"
test::assert_test_case "web::get_suitable_uid" "/opt/ServerKit/tests/cases/get_suitable_uid-4.txt"
test::assert_test_case "web::get_suitable_uid" "/opt/ServerKit/tests/cases/get_suitable_uid-5.txt"
