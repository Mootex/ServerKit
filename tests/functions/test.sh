#!/usr/bin/env bash
# Requires: functions/random.sh

test::assert_test_case() {
    # Runs a test case.
    # $1: Command to be tested.
    # $2: File where the test case is stored, to be used as input for the
    #     command. The file must contain the expected result in the first
    #     line. Second line will be discarded, and from the third it will
    #     be used as the input file for the test.

    # Get expected result
    local expected=$(head -n 1 $2)

    # Get test case contentx
    local test_case=$(tail -n +3 $2)

    # Create temporary test case
    local temp_file_name="$(random::temp_file test_case_)"

    # Write temp test case
    echo "$test_case" > "$temp_file_name"

    # Pick test date
    local now=$(date +"%Y-%m-%d %H:%M:%S")

    # Run the command
    local actual=$($1 "$temp_file_name")

    # Assert!
    if [ "$actual" != "$expected" ]; then
        echo ""
        echo "[$now] TEST FAILED: $1 '$2'"
        echo -n "  WHERE: "; caller
        echo "  EXPECTED: $expected"
        echo "  ACTUAL:   $actual"
        echo ""
    else
        echo "[$now] TEST OK: $1 '$2'"
    fi

    # Remove temp file
    unlink "$temp_file_name"
}
